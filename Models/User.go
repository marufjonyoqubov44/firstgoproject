package Models

import (
	"akam/Config"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-jwt/jwt"
)

type UserValidate struct {
    Name string `json:"name" binding:"required"`
    Station_id int `json:"station_id" binding:"required"`
    Login    string `json:"login" binding:"required,min=5"`
    Password string `json:"password" binding:"required,min=5"`
}

type UserLogin struct {
    Login    string `json:"login" binding:"required,min=5"`
    Password string `json:"password" binding:"required,min=5"`
}

type UserCreate struct {
    Name string `json:"name"`
    Station_id int `json:"station_id"`
    Login    string `json:"login"`
    Password string `json:"password"`
	Created_at int `json:"created_at"`
}

type UserJwt struct {
    Name string `json:"name"`
    Station_id int `json:"station_id"`
    Login    string `json:"login"`
    Password string `json:"password"`
	Created_at int `json:"created_at"`
	jwt.StandardClaims
}

type UserUpdate struct {
    Name string `json:"name"`
    Station_id int `json:"station_id"`
    Login    string `json:"login"`
    Password string `json:"password"`
	Created_at int `json:"created_at"`
}


type User struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
}

type UserInfo struct {
	Id       uint   `json:"id"`
	Name  string    `json:"name"`
	Station_id int `json:"station_id"`
	Login   string    `json:"login"`
	// Email   string `json:"email"`
	// Phone   string `json:"phone"`
	// Address string `json:"address"`
}

type UserInfoAll struct {
	Id       uint   `json:"id"`
	Name  string    `json:"name"`
	Station_id int `json:"station_id"`
	Login   string    `json:"login"`
	Text     string `json:"text"`
	// Phone   string `json:"phone"`
	// Address string `json:"address"`
}

type UserInfoPagination struct {
	User  []UserInfo `json:"user"`
	Page  int           `json:"page"`
	Size  int           `json:"size"`
	Count int           `json:"count"`
}

func (b *User) TableName() string {
	return "user"
}


//GetAllUsers Fetch all user data
func GetAllUsers(user *[]User) (err error) {
	if err = Config.DB.Find(user).Error; err != nil {
		return err
	}
	return nil
}

//CreateUser ... Insert New data
func CreateUser(user *User) (err error) {
	if err = Config.DB.Create(user).Error; err != nil {
		return err
	}
	return nil
}

//GetUserByID ... Fetch only one user by Id
func GetUserByID(user *User, id string) (err error) {
	if err = Config.DB.Where("id = ?", id).First(user).Error; err != nil {
		return err
	}
	return nil
}

//UpdateUser ... Update user
func UpdateUser(user *User, id string) (err error) {
	fmt.Println(user)
	Config.DB.Save(user)
	return nil
}

//DeleteUser ... Delete user
func DeleteUser(user *User, id string) (err error) {
	Config.DB.Where("id = ?", id).Delete(user)
	return nil
}
