package Controllers

import (
	"akam/Config"
	"akam/Models"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"github.com/golang-jwt/jwt"
)

// //GetUsers ... Get all users
// func GetUsers(c *gin.Context) {
// 	var user []Models.User
// 	err := Models.GetAllUsers(&user)
// 	if err != nil {
// 		c.AbortWithStatus(http.StatusNotFound)
// 	} else {
// 		c.JSON(http.StatusOK, user)
// 	}
// }

//CreateUser ... Create User
func UserCreate(c *gin.Context) {
	var user Models.UserValidate
	err := c.ShouldBindJSON(&user)
	if err != nil {
		fmt.Println("register failed")
		c.JSON(400, gin.H{"message": err.Error()})
		return
	}

	var user_create Models.UserCreate
	var password, _ = bcrypt.GenerateFromPassword([]byte(user.Password), 10)

	user_create.Login = user.Login
	user_create.Password = string(password)
	user_create.Name = user.Name
	user_create.Station_id = user.Station_id
	user_create.Created_at = time.Now().Second()
	// bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))

	Config.DB.Table("user").Create(user_create)
	c.JSON(200, gin.H{"message": "Foydalanuvchi qo'shildi"})
}

func CrateToken(c *gin.Context) {
	mySigningKey := []byte("Maruf")

	var userJwt = Models.UserJwt{}
	// Create the Claims
	userJwt.StandardClaims = jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 12).Unix(),
		Issuer:    "Maruf",
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, userJwt)
	ss, _ := token.SignedString(mySigningKey)
	c.JSON(200, ss)
}

func CheckToken(c *gin.Context) {
	var hmacSampleSecret = []byte("Maruf")
	var tokenString = c.GetHeader("Authorization")
	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return hmacSampleSecret, nil
	})
	c.JSON(http.StatusOK, token.Claims)

	// if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
	// 	fmt.Println(claims["foo"], claims["nbf"])
	// } else {
	// 	fmt.Println(err)
	// }
}

func UserLogin(c *gin.Context) {
	var user Models.UserLogin
	err := c.ShouldBindJSON(&user)
	if err != nil {
		fmt.Println("register failed")
		c.JSON(400, gin.H{"message": err.Error()})
		return
	}

	var user_login Models.UserLogin
	Config.DB.Table("user").Where("login = ?", user.Login).First(&user_login)

	if user_login.Login == "" {
		c.JSON(401, gin.H{"message": "Sizga ruxsat yo'q"})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user_login.Password), []byte(user.Password))
	if err != nil {
		c.JSON(401, gin.H{"message": "Sizga ruxsat yo'q"})
		return
	}
	c.JSON(200, gin.H{"message": "Muaffaqiyatli kirildi"})
}

func UserInformation(c *gin.Context) {
	var users []Models.UserInfo
	var userAll []Models.UserInfoAll
	// //    var userone Models.UserInfo

	Config.DB.Table("user").Limit(10).Order("rand()").Find(&users)

	for _, value := range users {
		var userOne Models.UserInfoAll
		userOne.Id = value.Id
		userOne.Login = value.Login
		userOne.Name = value.Name
		userOne.Station_id = value.Station_id
		userOne.Text = "User"
		if value.Station_id > 1 {
			userOne.Text = "Admin"
		}
		userAll = append(userAll, userOne)
	}

	// //    user_one := Config.DB.Table("user").Where("id = ?", 1).Find(&userone).Value

	//    c.JSON(http.StatusOK, users)

	//    rows:= Config.DB. // Note: Ignoring errors for brevity
	// for users.Next() {
	// 	var userone Models.UserInfo
	// 	users.Scan(&userone)
	// 	fmt.Println(userone)
	// 	// Do something with 's'

	// }
	c.JSON(http.StatusOK, userAll)
}

func UserPagination(c *gin.Context) {
	var page, _ = strconv.Atoi(c.Query("page"))
	var size, _ = strconv.Atoi(c.Query("size"))
	var users []Models.UserInfo
	var userCount int
	var pagination Models.UserInfoPagination

	if page == 0 {
		page = 1
	}
	if size == 0 {
		size = 10
	}

	Config.DB.Table("user").Limit(size).Offset((page - 1) * size).Find(&users)
	Config.DB.Table("user").Count(&userCount)

	pagination.Page = page
	pagination.Size = size
	pagination.Count = userCount
	pagination.User = users

	c.JSON(http.StatusOK, pagination)

}
