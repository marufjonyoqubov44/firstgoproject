package Routes

import (
	"akam/Controllers"

	"github.com/gin-gonic/gin"
)

//SetupRouter ... Configure routes
func SetupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("hello", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Hello World",
		})
	})
	r.GET("information", Controllers.UserInformation)
	r.GET("pagination", Controllers.UserPagination)
	grp1 := r.Group("/user")
	{
		// grp1.GET("", Controllers.GetUsers)
		grp1.POST("", Controllers.UserCreate)
		grp1.POST("/login", Controllers.UserLogin)
		// grp1.GET("/:id", Controllers.GetUserByID)
		// grp1.PUT("/:id", Controllers.UpdateUser)
		// grp1.DELETE("/:id", Controllers.DeleteUser)
	}
	r.GET("token-create", Controllers.CrateToken)
	r.GET("token-check", Controllers.CheckToken)
	return r
}
